/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
 import React from 'react';
 import {
   SafeAreaView,
   ScrollView,
   StatusBar,
   StyleSheet,
   Text,
   View,
 } from 'react-native';
 
 import AppNavigator from './src/navigation';
 import { AuthProvider } from './src/navigation/AuthProvider';
 
 const App = () => {
 
   return (
     <AuthProvider>
     <AppNavigator />
     </AuthProvider>
   );
 };
 
 const styles = StyleSheet.create({
 
 });
 
 export default App;
 