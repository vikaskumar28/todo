import React, { useState, useEffect, useContext } from "react";
import {View, Text, Button, TextInput, Alert, FlatList, TouchableOpacity} from 'react-native';
import { AuthContext } from "../navigation/AuthProvider";

const HomeScreen = (props) => {

    const [todoItems, setTodoItems] = useState([]);
    const [todoText, setTodoText] = useState('');

    const {user, logout} = useContext(AuthContext);

    console.log('user', user);

    const addTodoItem = () => {
        if (todoText.length > 2) {
            setTodoItems(prevItems => [...prevItems, todoText]);
            setTodoText('');
        } else {
            Alert.alert('Error!', 'Task length must be greater than 2 characters');
        }
    };

    const deleteTask = (index) => {
        setTodoItems(prevItem => [...prevItem.slice(0, index), ...prevItem.slice(index + 1)]);
    };

    const renderTodoList = ({item, index}) => {
        return (
            <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                <Text style={{fontSize: 18}}>{index + 1}. {item}</Text>
                <TouchableOpacity onPress={() => deleteTask(index)}>
                    <Text style={{fontSize: 20, color: 'red', fontWeight: 'bold'}}>X</Text>
                </TouchableOpacity>
            </View>
        );
    };

    return (
        <View style={{ borderWidth: 1, flex: 1, paddingVertical: 20, paddingHorizontal: 30}}>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: '100%'}}>
                <TextInput
                    placeholder="Enter Task"
                    value={todoText}
                    onChangeText={value => setTodoText(value)}
                    style={{borderBottomWidth: 1, width: '70%', fontSize: 18}}
                />
                <Button
                    title="Add"
                    onPress={addTodoItem}
                />
            </View>
            <FlatList
                data={todoItems}
                renderItem={renderTodoList}
                style={{marginTop: 30}}
            />
            <View>
                <Button
                    title="Logout"
                    onPress={() => logout()}
                />
            </View>
        </View>
    );
};

export default HomeScreen;