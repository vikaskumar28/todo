import React, { useState, useEffect, useContext } from "react";
import {View, Text, Button, TextInput, TouchableWithoutFeedback, StyleSheet} from 'react-native';

import { AuthContext } from "../navigation/AuthProvider";

const LoginScreen = (props) => {

    const [signupData, setSignupData] = useState({email: '', password: ''});

    const {register} = useContext(AuthContext);

    return (
        <View style={{ flex: 1, paddingHorizontal: 30, justifyContent: 'center'}}>
            <View style={{}}>
                <TextInput
                    placeholder="Enter email"
                    value={signupData.email}
                    onChangeText={value => setSignupData(prevData => ({...prevData, email: value}))}
                    style={styles.input}
                />
                <TextInput
                    placeholder="Enter password"
                    value={signupData.password}
                    onChangeText={value => setSignupData(prevData => ({...prevData, password: value}))}
                    style={styles.input}
                    secureTextEntry={true}
                />
                <Button
                    title="Sign Up"
                    onPress={() => register(signupData.email, signupData.password)}
                />
            </View>
            <View style={{flexDirection: 'row', marginTop: 20, justifyContent: 'center'}}>
                <Text style={styles.text}>Already registerd? </Text>
                <TouchableWithoutFeedback onPress={() => props.navigation.navigate('Login')}>
                    <Text style={[styles.text, {color: 'blue'}]}>Login</Text>
                </TouchableWithoutFeedback>
            </View>
        </View>
    );
};

export default LoginScreen;

const styles = StyleSheet.create({
    input: {
        marginVertical: 10,
        borderBottomWidth: 1,
        fontSize: 18
    },
    text: {
        fontSize: 18
    }
});