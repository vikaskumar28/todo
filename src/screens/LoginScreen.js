import React, { useState, useEffect, useContext } from "react";
import {View, Text, Button, TextInput, TouchableWithoutFeedback, StyleSheet} from 'react-native';
import { AuthContext } from "../navigation/AuthProvider";

const LoginScreen = (props) => {

    const [loginData, setLoginData] = useState({email: '', password: ''});

    const {login} = useContext(AuthContext);
    
    return (
        <View style={{ flex: 1, paddingHorizontal: 30, justifyContent: 'center'}}>
            <View style={{}}>
                <TextInput
                    placeholder="Enter email"
                    value={loginData.email}
                    onChangeText={value => setLoginData(prevData => ({...prevData, email: value}))}
                    style={styles.input}
                />
                <TextInput
                    placeholder="Enter password"
                    value={loginData.password}
                    onChangeText={value => setLoginData(prevData => ({...prevData, password: value}))}
                    style={styles.input}
                />
                <Button
                    title="Login"
                    onPress={() => login(loginData.email, loginData.password)}
                />
            </View>
            <View style={{flexDirection: 'row', marginTop: 20, justifyContent: 'center'}}>
                <Text style={styles.text}>Don't have an account. </Text>
                <TouchableWithoutFeedback onPress={() => props.navigation.navigate('Signup')}>
                    <Text style={[styles.text, {color: 'blue'}]}>Sign Up</Text>
                </TouchableWithoutFeedback>
            </View>
        </View>
    );
};

export default LoginScreen;

const styles = StyleSheet.create({
    input: {
        marginVertical: 10,
        borderBottomWidth: 1,
        fontSize: 18
    },
    text: {
        fontSize: 18
    }
});